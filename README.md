
## Square1 test


- Copy the `.env.example` to `.env`  and set the variables depending on your system 
- Run `composer install` then `npm install` and then `npm run dev`
- Now, in order to seed your database with fake data, just run `php artisan migrate --seed` 

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
