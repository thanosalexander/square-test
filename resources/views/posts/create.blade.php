@php

    $styles = [
        // daterange picker
        '/vendor/daterangepicker/daterangepicker.css',
    ];

    $scripts = [
        '/vendor/moment/moment-with-locales.js',
        // daterange picker
        '/vendor/daterangepicker/daterangepicker.js',
    ];


@endphp


@extends('layouts.app')

@push('styles')
    @foreach($styles as $style)
        <link href="{{  asset($style) }}" rel="stylesheet" type="text/css">
    @endforeach
@endpush


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5>Add new post</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('posts.store') }}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input name="title" placeholder="Insert post's title" id="title" value="{{ old('title') }}" type="text" class="form-control @error("title") is-invalid @enderror"  />

                                @error("title") <span class="invalid-feedback">{{ $message }}</span>@enderror
                            </div>

                            <div class="form-group">
                                <label for="title">Title</label>
                                <textarea name="description" placeholder="Insert post's description" id="description"   class="form-control @error("description") is-invalid @enderror" >{{ old('description') }}</textarea>

                                @error("title") <span class="invalid-feedback">{{ $message }}</span>@enderror
                            </div>


                            <div class="form-group">
                                <label for="publication_date">Publication Date</label>
                                <input autocomplete="off" type="text" class="form-control @error("publication_date") is-invalid @enderror" id="publication_date" name="publication_date" value="{{ old('publication_date') }}">
                                @error("publication_date") <span class="invalid-feedback">{{ $message }}</span>@enderror
                            </div>


                            <div class="form-group d-flex justify-content-between">
                                <a href="{{ route('posts.index') }}" class="btn btn-warning btn-sm">
                                    Cancel
                                </a>
                                <button type="submit"  class="btn btn-success btn-sm">
                                    Save
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

    @foreach($scripts as $script)
        <script type="text/javascript" src="{{ asset($script)}}"></script>
    @endforeach

    <script type="text/javascript">

        $(document).ready((e)=>{
            const DATE_FORMAT = 'YYYY/MM/DD HH:mm:ss'
            $('#publication_date').daterangepicker({
                singleDatePicker: true,
                timePicker24Hour:true,
                timePicker: true,
                showDropdowns:true,
                autoUpdateInput: true,
                locale: {
                    format: DATE_FORMAT,
                }
            });
        })

    </script>
@endpush
