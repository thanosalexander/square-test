<div class="p-2">

    <h1 class="mb-5">Posts</h1>


    @php /** @var \App\Post $post */ @endphp
    @foreach ($posts as $post)

        <article class="mb-4 @if(!$loop->last) border-bottom border-dark @endif" >
            <h2 style="margin-bottom: 0;">{{$post->title}}</h2>
            <small class="italic">
                {{ $post->publication_date->ago() }}
            </small>
            <p>
                {{ $post->description }}
            </p>

        </article>
    @endforeach

    {{ $posts->links() }}


</div>
