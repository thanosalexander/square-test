@php

    $styles = [
        // DataTables
        '/vendor/datatables-bs4/css/dataTables.bootstrap4.min.css',
        '/vendor/datatables-responsive/css/responsive.bootstrap4.min.css',
    ];

    $scripts = [
          // DataTables
        '/vendor/datatables/jquery.dataTables.min.js',
        '/vendor/datatables-bs4/js/dataTables.bootstrap4.min.js',
        '/vendor/datatables-responsive/js/dataTables.responsive.min.js',
        '/vendor/datatables-responsive/js/responsive.bootstrap4.min.js',
    ];


@endphp

@push('styles')
    @foreach($styles as $style)
        <link href="{{  asset($style) }}" rel="stylesheet" type="text/css">
    @endforeach
@endpush

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>Posts</h5>
                    <div class="btn-group btn-group-sm">
                        @if(auth()->user()->isTypeOf(\App\User::ADMIN_TYPE))
                            <button type="button" wire:click="fetchRemote" class="btn btn-sm btn-primary">
                                Fetch remote
                            </button>
                        @endif
                        <a class="btn btn-sm btn-info" href="{{ route('posts.create') }}">
                            Add Post
                        </a>
                    </div>
                </div>
                <div class="card-body" wire:ignore>
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Publication date</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    @foreach($scripts as $script)
        <script type="text/javascript" src="{{ asset($script)}}"></script>
    @endforeach
    <script type="text/javascript">

        $(document).ready((e)=>{
            let table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive:true,
                stateSave: true,
                ajax: "{{ route('posts.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'description', name: 'description'},
                    {data: 'publication_date', name: 'publication_date'},
                ]
            });


            window.livewire.on('refreshTable',()=>{
                table.ajax.reload()
            })
        })

    </script>
@endpush
