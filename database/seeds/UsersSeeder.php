<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->state('admin')->create([
            'email'=>'admin@square.test'
        ]);

        factory(User::class,10)->create([
            'type'=>User::USER_TYPE
        ]);
    }
}
