<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::ofType(User::USER_TYPE)->get() as $user) {
            factory(Post::class,10)->create([
                'user_id'=>$user->id
            ]);
        }

        //Seed once
        $posts = json_decode(file_get_contents("https://sq1-api-test.herokuapp.com/posts"),true);
        $adminUser = User::ofType(User::ADMIN_TYPE)->first();
        foreach (Arr::get($posts, 'data') as $postData) {
            factory(\App\Post::class )->create([
                'user_id'=>$adminUser->id,
                'title' => Arr::get($postData, 'title'),
                'description' => Arr::get($postData, 'description'),
                'publication_date'=> Arr::get($postData, 'publication_date'),
            ]);
        }
    }
}
