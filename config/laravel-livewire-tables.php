<?php

return [

    /*
     * The frontend styling framework to use
     * Options: bootstrap-4
     */
    'theme' => 'bootstrap-4',
];
