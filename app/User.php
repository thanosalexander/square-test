<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    public const ADMIN_TYPE = 'admin';
    public const USER_TYPE = 'user';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @param Builder $users
     * @param string $type
     * @return Builder
     */
    public function scopeOfType(Builder $users, string $type): Builder
    {
        if (!in_array($type, [
            self::ADMIN_TYPE,
            self::USER_TYPE
        ], true)) return $users;

        return $users->where('type', '=', $type);
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isTypeOf(string $type): bool
    {
        if (!in_array($type, [
            self::ADMIN_TYPE,
            self::USER_TYPE
        ], true)) return false;

        return $this->type === $type;
    }
}
