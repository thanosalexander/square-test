<?php

namespace App\Http\Controllers;

use App\Post;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = auth()->user()->posts();
            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('publication_date', function (Post $post) {
                    return $post->publication_date->format('d/m/Y H:i:s');
                })
                ->make(true);
        }

        return view('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'title' => [
                'required', 'string', 'max:255'
            ],
            'description' => [
                'required', 'string', 'max:5000'
            ],
            'publication_date' => [
                'required', 'string', 'max:255', 'date_format:Y/m/d H:i:s'
            ],
        ]);


        DB::beginTransaction();
        try {
            $validatedData['user_id'] = auth()->user()->id;

            Post::create($validatedData);

            DB::commit();

            toastr()->success('Post created successfully!');

            return redirect()->route('posts.index');
        } catch (Exception $exception) {
            DB::rollBack();

            toastr()->error('Post could not be created!');
        }
    }

}
