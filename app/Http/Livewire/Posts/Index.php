<?php

namespace App\Http\Livewire\Posts;

use App\Post;
use Illuminate\Support\Arr;
use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.posts.index');
    }

    public function fetchRemote()
    {
        $posts = json_decode(file_get_contents("https://sq1-api-test.herokuapp.com/posts"),true);
        $adminUser =auth()->user();
        foreach (Arr::get($posts, 'data') as $postData) {
            Post::create([
                'user_id'=>$adminUser->id,
                'title' => Arr::get($postData, 'title'),
                'description' => Arr::get($postData, 'description'),
                'publication_date'=> Arr::get($postData, 'publication_date'),
            ]);
        }


        $this->emit('refreshTable');
    }
}
